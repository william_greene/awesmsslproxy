<?php

$api_call = curl_init();
curl_setopt($api_call, CURLOPT_URL, 'http://widgets.awe.sm/v2/widgets.js?key='.$_GET['key']);
curl_setopt($api_call, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($api_call, CURLOPT_TIMEOUT, 10);
$api_response = curl_exec($api_call);
$api_response_code = curl_getinfo($api_call, CURLINFO_HTTP_CODE);
curl_close($api_call);

// Set response code
header('', true, $api_response_code);

// Exit on unhappy response
if ($api_response_code >= 400) {
	die();
}

// Find and replace references to non-SSL awe.sm endpoints
// -------------------------------------------------------

$proxied_response = $api_response;

// http://widgets.awe.sm/v1/asyncshare/register
// http://widgets.awe.sm/v1/asyncshare/click
// http://widgets.awe.sm/v2/fbshare_button
// http://widgets.awe.sm/v2/tweet_button
$proxied_response = str_replace('widgets.awe.sm',	'awesmsslproxy.herokuapp.com', $proxied_response);

// http://api.awe.sm/conversions/new
$proxied_response = str_replace('api.awe.sm',		'awesmsslproxy.herokuapp.com', $proxied_response);


// Done! Set body
header('Content-type: text/javascript');
echo $proxied_response;