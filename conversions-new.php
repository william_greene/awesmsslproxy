<?php

$api_call = curl_init();
curl_setopt($api_call, CURLOPT_URL, 'http://api.awe.sm/conversions/new');
curl_setopt($api_call, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($api_call, CURLOPT_TIMEOUT, 10);
curl_setopt($api_call, CURLOPT_POST, 1);
curl_setopt($api_call, CURLOPT_POSTFIELDS, $_REQUEST);
$api_response = curl_exec($api_call);
$api_response_code = curl_getinfo($api_call, CURLINFO_HTTP_CODE);
$api_response_mime = curl_getinfo($api_call, CURLINFO_CONTENT_TYPE);
curl_close($api_call);

// Set header
header('Cache-Control: no-cache, must-revalidate');
header('Expires: '.gmdate('D, d M Y H:i:s', time()).' GMT');

// Set response code
header('', true, $api_response_code);

// Set body
header('Content-type: '.$api_response_mime);
echo $api_response;