<?php

// http://widgets.awe.sm/v1/asyncshare/register
// http://widgets.awe.sm/v1/asyncshare/click
// http://widgets.awe.sm/v2/fbshare_button
// http://widgets.awe.sm/v2/tweet_button

$api_call = curl_init();
curl_setopt($api_call, CURLOPT_URL, 'http://widgets.awe.sm'.$_SERVER['REQUEST_URI']);
curl_setopt($api_call, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($api_call, CURLOPT_TIMEOUT, 10);
curl_setopt($api_call, CURLOPT_POST, 1);
curl_setopt($api_call, CURLOPT_POSTFIELDS, $_REQUEST);
$api_response = curl_exec($api_call);
$api_response_code = curl_getinfo($api_call, CURLINFO_HTTP_CODE);
$api_response_mime = curl_getinfo($api_call, CURLINFO_CONTENT_TYPE);
curl_close($api_call);

// Set response code
header('', true, $api_response_code);
header('Content-type: '.$api_response_mime);

// Set body
echo $api_response;