DESCRIPTION
===========

This acts as an SSL proxy to awe.sm's non-SSL endpoints.
It proxies the conversions/new endpoint.
It proxies the awe.sm-powered Facebook Like button and related endpoints.


HOW IT WORKS
============

.htaccess
- pass thru /v2/widgets.js?key=XXXXXXXXXX to widgets.php
- pass thru /conversions/new to conversions-new.php

conversions-new.php
- requests http://api.awe.sm/conversions/new with URL parameters
- returns response with JSON headers

widgets-js.php
- requests http://widgets.awe.sm/v2/widgets.js?key=XXXXXXXXXX
- replaces http://widgets.awe.sm/ with https://awesmsslproxy.heroku.com/

widgets.php
- simple proxies for widgets.awe.sm requests used by widgets.js


NOTES
=====
https://awesmsslproxy.herokuapp.com/test.html?awesm=awe.sm_5WXHo

Original Heroku name: severe-stone-3918